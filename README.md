# Test CI

Projet Test - Gitlab CI

- Le fichier `prefixer.js` contient une fonction utilitaire.
- Le fichier `test/test.js` est un ensemble de Test Unitaires (basé sur [mocha](https://mochajs.org/)) de cette fonction.

## Lancement des tests

```
$ npm test
```
