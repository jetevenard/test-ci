/**
 * Préfixe une chaine.
 */
function prefix(prefix, contents) {
    return `[${prefix.toUpperCase()}] ${contents}`;
}

module.exports = prefix;